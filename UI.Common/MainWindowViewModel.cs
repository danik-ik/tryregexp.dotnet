﻿using System;
using System.Collections.Generic;
using System.Reactive.Linq;
using System.Text.RegularExpressions;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;

namespace UI.Common.ViewModels
{
    public class MainWindowViewModel: ViewModelBase
    {
        [Reactive] public string Pattern { get; set; } = "";
        [Reactive] public string Input { get; set; } = "";
        private readonly ObservableAsPropertyHelper<ICollection<MatchViewModel>> _result;
        public ICollection<MatchViewModel> Result => _result.Value;

        public MainWindowViewModel()
        {
            this.WhenAnyValue(vm => vm.Pattern, vm => vm.Input)
                .Throttle(TimeSpan.FromSeconds(1))
                .Select(v => ProcessInput(v.Item1, v.Item2))
                .ToProperty(this, vm => vm.Result, out _result);
        }

        private static ICollection<MatchViewModel> ProcessInput(string pattern, string input)
        {
            List<MatchViewModel> it = new();

            if (string.IsNullOrEmpty(pattern)) return it;
            if (string.IsNullOrEmpty(input)) return it;

            try
            {
                Regex rg = new Regex(pattern);
                MatchCollection matches = rg.Matches(input);

                foreach (Match m in matches)
                {
                    it.Add(new MatchViewModel(m));
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                it.Clear();
            }
            return it;
        }

    }
}