using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace UI.Common.ViewModels
{
    public class MatchViewModel: ViewModelBase
    {
        private readonly Match _match;

        public MatchViewModel(Match match)
        {
            _match = match;
        }

        public string Name => "—";
        public string Value => _match.Value;
        public int Index => _match.Index;
        public ICollection<GroupViewModel> Groups =>
            ((IList<Group>) _match.Groups)
                .Select(g => new GroupViewModel(g))
                .ToList();
    }
}