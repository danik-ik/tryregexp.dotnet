using System.Text.RegularExpressions;

namespace UI.Common.ViewModels
{
    public class GroupViewModel
    {
        private readonly Group _group;

        public GroupViewModel(Group group)
        {
            _group = group;
        }
        
        public string Name => _group.Name;
        public string Value => _group.Value;
        public int Index => _group.Index;
    }
}