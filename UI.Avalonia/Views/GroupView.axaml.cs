using Avalonia.Controls;
using Avalonia.Markup.Xaml;

namespace UI.Avalonia.Views
{
    public class GroupView : UserControl
    {
        public GroupView()
        {
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}