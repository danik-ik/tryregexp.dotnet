using Avalonia.Controls;
using Avalonia.Markup.Xaml;

namespace UI.Avalonia.Views
{
    public class MatchView : UserControl
    {
        public MatchView()
        {
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}