using System.Text.RegularExpressions;
using UI.Common.ViewModels;

namespace UI.Avalonia.DesignViewModels
{
    public class MatchDesignViewModel:MatchViewModel
    {
        public MatchDesignViewModel(): base(MatchFactory())
        {
        }

        private static Match MatchFactory()
        {
            var pattern = @"(.|\n)+";
            var input = @"WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW
WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW
WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW
";
            var regexp = new Regex(pattern);
            return regexp.Match(input);
        }
    }
}