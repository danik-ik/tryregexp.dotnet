using System.Text.RegularExpressions;
using UI.Common.ViewModels;

namespace UI.Avalonia.DesignViewModels
{
    public class GroupDesignViewModel: GroupViewModel
    {
        public GroupDesignViewModel() : base(GroupFactory())
        {
        }

        private static Group GroupFactory()
        {
            var pattern = @"(?<it>(.|\n)+)";
            var input = @"WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW
WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW
WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW
";
            var regexp = new Regex(pattern);
            return regexp.Match(input).Groups["it"];
        }
    }
}